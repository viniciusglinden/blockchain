/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Basic tests.
 * @todo
 * https://github.com/catchorg/Catch2/blob/devel/examples/231-Cfg-OutputStreams.cpp
 */

#include <cstdint>
#define CATCH_CONFIG_MAIN
#include <stdint.h>

#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "blockchain.h"
#include "hash.h"
#include "io.h"
#include "sha-256.h"
}

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

TEST_CASE("read file") {
    char* test = file_read("README.md");
    REQUIRE(test != NULL);
    puts(test); // visually test for now
    free(test);
}

TEST_CASE("basic hash test and example") {
    // echo -n "abc" | sha256sum
    const uint8_t correct[] = {0xBA, 0x78, 0x16, 0xBF, 0x8F, 0x01, 0xCF, 0xEA,
                               0x41, 0x41, 0x40, 0xDE, 0x5D, 0xAE, 0x22, 0x23,
                               0xB0, 0x03, 0x61, 0xA3, 0x96, 0x17, 0x7A, 0x9C,
                               0xB4, 0x10, 0xFF, 0x61, 0xF2, 0x00, 0x15, 0xAD};
    REQUIRE(32 == ARRAY_SIZE(correct));

    uint8_t hash[32];
    calc_sha_256(hash, "abc", strlen("abc"));

    for (size_t i = 0; i < 32; ++i)
        REQUIRE(hash[i] == correct[i]);
}

TEST_CASE("bash constant block") {
    block_t block_1;
    memset(&block_1, 0xAA, sizeof(block_t));
    const hash_t hash_a = hash_sha256(&block_1);
    const hash_t hash_b = hash_sha256(&block_1);
    REQUIRE(hash_a == hash_b);

    block_t block_2;
    memset(&block_2, 0xAB, 1);
    const hash_t hash_c = hash_sha256(&block_2);
    REQUIRE(hash_a != hash_c);
}

SCENARIO("normal blockchain") {
    GIVEN("the blockchain has been correctly initialized") {
        blockchain_t bc = blockchain_init(5, &hash_sha256);
        THEN("there are no blocks in the blockchain") {
            REQUIRE(blockchain_length(&bc) == 0);
        }
        WHEN("blocks are added") {
            const uint8_t nblocks = 5;
            for (uint8_t i = 0; i < nblocks; ++i)
                blockchain_block_add(&bc, (char*)"hehe");
            THEN("Length is the number of blocks") {
                REQUIRE(blockchain_length(&bc) == nblocks);
            }
            THEN("blockchain is verified") {
                REQUIRE(blockchain_validate(&bc, UINT8_MAX));
            }
            WHEN("block is adulterated") {
                block_t* block = blockchain_block_get(&bc, -1);
                REQUIRE(block);
                block_message(block, "lol");
                THEN("blockchain is not verifiable") {
                    REQUIRE(!blockchain_validate(&bc, UINT8_MAX));
                }
            }
        }
        blockchain_kill(bc);
    }
}

struct test_struct_t {
    int a, b, c;

   public:
    bool operator==(test_struct_t other) {
        return (this->a == other.a) && (this->b == other.b) &&
               (this->c == other.c);
    }
    friend std::ostream& operator<<(std::ostream& os, const test_struct_t& obj);
};

std::ostream& operator<<(std::ostream& os, const test_struct_t& obj) {
    os << "test_struct_t {"
       << "\n\t.a = " << obj.a << "\n\t.b = " << obj.b << "\n\t.c = " << obj.c
       << "\n}";
    return os;
}
SCENARIO("IO operations") {
    GIVEN("we have a block that we would like to write") {
        const char name[] = "test.data";
        test_struct_t expected{1, 2, 3};
        THEN("we write this block to a file as a byte") {
            io_operation_byte(&expected, sizeof(test_struct_t), name, write_e);
        }
        WHEN("we read this file back") {
            test_struct_t read;
            memset(&read, 0, sizeof(test_struct_t));
            io_operation_byte(&read, sizeof(test_struct_t), name, read_e);
            THEN("read back is the same as the written file") {
                REQUIRE(read == expected);
            }
        }
    }
}
