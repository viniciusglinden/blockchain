
# Serialization

## JSON

A block in the blockchain in JSON format looks like:

```json
{
    "hash previous": "integer",
    "nounce": "integer",
    "message": "string"
}
```

The whole blockchain looks like:

```json
{
    "hash function": "string",
    "zero bits": "integer",
    "blocks": [
    ]
}
```

<!--
TODO implement a more generic hash_function
```json
"hash function":
    "function": "string",
    "parameters": []
    "length": "integer"
}
```
-->
