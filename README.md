
# Blockchain

![pipeline](https://gitlab.com/viniciusglinden/blockchain/badges/master/pipeline.svg)

Simple blockchain implementation in C to learn more about blockchain.

[Gitlab repository](https://gitlab.com/viniciusglinden/blockchain)

## Interacting with the project

Type `./build.sh help` on the terminal to see how to interact with it.

## Code

I consider `*alloc` / the heap as dangerous. This is why I try to avoid it. This
is why the blockchain init function gives the handler as the return value.

## Hash algorithms

This is a useless toy project: it does not require safe-cryptography.

## References

- [But how bitcoin actually works?](https://www.youtube.com/watch?v=bBC-nXj3Ng4)
- [Self-contained SHA-256 implementation](https://github.com/amosnier/sha-2)
- [Return a struct from a function in
  C](https://stackoverflow.com/questions/9653072/return-a-struct-from-a-function-in-c)
