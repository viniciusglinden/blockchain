/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Project-wide codes and error treatment.
 * @details This imitates "<errno>".
 */

#ifndef CODES_H
#define CODES_H

#include <stdbool.h>

#include "debug.h"

//! Error code.
typedef enum code_t {
    reset_e, //!< Reset state.
    nullptr_e, //!< Pointer to NULL.
    allocate_e, //!< Failed to allocate.
    maximum_reached_e, //!< Maximum value reached.
    parameter_invalid_e, //!< Invalid parameter.
    block_null_e, //!< Block points to NULL.
    hash_invalid_e, //!< Invalid hash.
    io_failed_e, //!< Failed requested operation.
    blockchain_not_valid_e, //!< Blockchain is not valid.
    json_failed_e, //!< json conversion failed.
    file_e, //!< File operation failed.
} code_t;

/**
 * Get error string.
 * @param[in] code Error code.
 * @return String.
 */
const char* code_string_get(code_t code);

/**
 * Set error code.
 * @param[in] code Error code.
 */
void error_set(code_t code);

/**
 * Set error and print the code.
 * @param[in] code Error code.
 */
#define ERROR_SET(code) \
    do { \
        error_set(code); \
        DEBUG_ERROR("%s", code_string_get(code)); \
    } while (false)

/**
 * Set warning code.
 * @param[in] code Error code.
 */
void warning_set(code_t code);

/**
 * Set warning and print the code.
 * @param[in] code Error code.
 */
#define WARNING_SET(code) \
    do { \
        warning_set(code); \
        DEBUG_WARN("%s", code_string_get(code)); \
    } while (false)

#endif /* CODES_H */
