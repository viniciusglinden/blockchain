/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-07-01
 * @brief Blockchain file serializer and deserializer.
 */

#include <stdio.h>

#include "block.h"

//! IO mode types.
typedef enum access_t {
    write_e, //!< write mode.
    append_e, //!< append mode.
    read_e //!< read mode.
} access_t;

/**
 * Generic IO operation for data to a file in byte format.
 * @note Byte operations are not recommended.
 * User is responsible to allocate the data size.
 * @param[in,out] data Data.
 * @param[in] size Size of data.
 * @param[in] name Name of file.
 * @return Success status.
 * @retval true If successfully.
 */
bool io_operation_byte(void* data,
                       size_t size,
                       const char* name,
                       access_t mode);

/**
 * Generic write a string to a file.
 * @param[in] text Content to be written.
 * @param[in] name Name of file.
 * @param[in] overwrite Write access instead of append.
 * @return Success status.
 * @retval true If read successfully.
 */
bool io_operation(const char* text, const char* name, access_t mode);
