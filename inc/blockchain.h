/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Blockchain definition.
 */

#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include "block.h"
#include "hash.h"

//! Used hash function signature.
typedef hash_t (*hash_func_t)(block_t*);

//! Chain definition.
struct chain_t {
    block_t* block; //!< Pointer to block.
    struct chain_t* previous; //!< Previous chain.
    struct chain_t* next; //!< Next chain.
};
typedef struct chain_t chain_t;

//! Blockchain definition.
struct blockchain_t {
    hash_func_t hash_function; //!< Hash function to be used.
    /**
     * Number of zero bits required.
     * @details At the leftmost bit.
     */
    uint8_t zero_bits;
    chain_t* head; //!< First created chain.
    chain_t* tail; //!< Last created chain.
};

typedef struct blockchain_t blockchain_t;

/**
 * Initialize a blockchain.
 * @param[in] zero_bits Number of zero bits for a valid hash.
 * @param[in] hash_function Digest function.
 * @return Empty blockchain.
 */
blockchain_t blockchain_init(uint8_t zero_bits, hash_func_t hash_function);

/**
 * Add a block to the blockchain.
 * @details Maximum message size if @ref MESSAGE_SIZE .
 * @param[in,out] blockchain Blockchain handler.
 * @param[in] message Block content.
 * @return Success value.
 * @retval true If successful.
 */
bool blockchain_block_add(blockchain_t* blockchain, char* message);

/**
 * Validate the blockchain.
 * @param[in] blockchain Blockchain handler.
 * @param[in] depth How many chains to validate, starting from tail and going to
 *                  head.
 * @return Blockchain valid status.
 * @retval true If:
 * - blockchain is empty;
 * - blockchain is valid.
 * @retval false If:
 * - Blockchain invalid;
 * - Error occurred.
 */
bool blockchain_validate(const blockchain_t* blockchain, uint8_t depth);

/**
 * Add a block to the blockchain.
 * @param[in] blockchain Blockchain handler.
 * @param[in] index Maximum number of blocks to print, starting from the tail.
 */
void blockchain_print(const blockchain_t* blockchain, uint8_t index);

/**
 * Get amount of blocks inside the blockchain.
 * @param[in] blockchain Blockchain handler.
 * @return Blockchain length.
 * @retval UINT8_MAX on error.
 */
uint8_t blockchain_length(blockchain_t* blockchain);

/**
 * Get block pointer.
 * @param[in] blockchain Blockchain handler.
 * @param[in] index Index, positive values start from head; negative from tail.
 * @return Block corresponding to the index.
 * @retval NULL If block was not found.
 */
block_t* blockchain_block_get(blockchain_t* blockchain, int16_t index);

/**
 * Terminate the blockchain.
 * @note This is used when finishing the program.
 * @param[in,out] blockchain Blockchain handler.
 */
void blockchain_kill(blockchain_t blockchain);

/**
 * Serialize blcokchain to json file.
 * @param[in] blockchain Blockchain handler.
 * @param[in] path Path to file.
 */
void blockchain_to_json(blockchain_t blockchain, const char* path);

/**
 * Read blockchain from json file.
 * @note User is responsible to allocate blockchain.
 * @details If function fails, blockchain may be corrupted.
 * @param[out] blockchain Blockchain handler.
 * @param[in] path Path to file.
 * @return Success status.
 * @retval false Function failure.
 */
bool blockchain_from_json(blockchain_t* blockchain, const char* path);

#endif /* BLOCKCHAIN_H */
