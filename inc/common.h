/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Common definitions across the project.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stddef.h>
#include <stdint.h>

//! Byte code used for indication that something went wrong.
#define SPECIAL_BYTE UINT8_C(0xDE)

//! Width of hash.
typedef uint8_t hash_t;

//! Hash print specifier string.
#define HASH_SPECIFIER "%02X"

/**
 * Print an array as hexadecimal value.
 * @param[in] array Array to print.
 * @param[in] size Size of the array
 */
#define ARRAY_PRINT(array, size) array_print(array, #array, size)

/**
 * Print an array as hexadecimal value.
 * @note Use ARRAY_PRINT macro instead of calling this function.
 * @param[in] array Array to print.
 * @param[in] name Name of the array
 * @param[in] size Size of the array
 */
void array_print(const uint8_t array[], char* name, size_t size);

/**
 * Print number in binary form.
 * @note Altough %b is supported by some printf implementations, it is
 * non-standard.
 * @details
 * @code{.c}
 * printf("0b");
 * binary_get(2, 1));
 * putchar('\n');
 * @endcode
 * Will print "0b00000010"
 * @param[in] number Number to be printed.
 * @param[in] size Byte size of number to be printed.
 */
void binary_print(uint64_t number, size_t size);

/**
 * Read a whole file.
 * @param[in] path File path.
 * @returns File contents.
 * @retval NULL Operation fail.
 */
char* file_read(const char* path);

#endif /* COMMON_H */
