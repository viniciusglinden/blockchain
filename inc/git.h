/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-05
 * @brief Get git information.
 */

const char* git_version();
