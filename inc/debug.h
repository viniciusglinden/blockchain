/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Debug and color macros definition.
 */

#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <string.h>

/**
 * @defgroup colors Color strings
 * @{
 */

#define RESET_STYLE "\033[0m" //!< Reset color and bold.
#define BLACK "\033[30m" //!< Make the font black.
#define RED "\033[31m" //!< Make the font red.
#define GREEN "\033[32m" //!< Make the font green.
#define YELLOW "\033[33m" //!< Make the font yellow.
#define BLUE "\033[34m" //!< Make the font blue.
#define MAGENTA "\033[35m" //!< Make the font magenta.
#define CYAN "\033[36m" //!< Make the font cyan.
#define WHITE "\033[37m" //!< Make the font white.
#define BOLD "\033[1m" //!< Make the font bold.

//! @}

/**
 * @defgroup debug_files Debug macro files
 * You may change the files which the debug macros will print by defining
 * with precompiler flags.
 * @{
 */

#ifndef __DEBUG_INFO_FILE__
//! File stream to print debug information.
#define __DEBUG_INFO_FILE__ (stdout)
#endif /* __DEBUG_INFO_FILE__ */
#ifndef __DEBUG_WARN_FILE__
//! File stream to print warning information.
#define __DEBUG_WARN_FILE__ (stdout)
#endif /* __DEBUG_WARN_FILE__ */
#ifndef __DEBUG_ERROR_FILE__
//! File stream to print error information.
#define __DEBUG_ERROR_FILE__ (stderr)
#endif /* __DEBUG_ERROR_FILE__ */

//! @}

//! File name macro.
#define __DEBUGFILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

/**
 * Internal debug print.
 * @param[in] file File stream on which to write.
 * @param[in] type String to print at the beginning.
 * @param[in] format Format formatting.
 * @param[in] ... Variables cited in the format formatting.
 * @return Number of characters printed.
 */
#define __DEBUG__(file, type, format, ...) \
    fprintf(file, BOLD type " %s@%d %s()]: " RESET_STYLE format "\n", \
            __DEBUGFILENAME__, __LINE__, __func__, ##__VA_ARGS__)
/**
 * Internal debug printf variable.
 * @param[in] file File stream on which to write.
 * @param[in] type String to print at the beginning.
 * @param[in] var Variable to be printed.
 * @return Number of characters printed.
 */
#define __DEBUG_INT__(file, type, var) \
    fprintf(file, BOLD type " %s@%d %s()]: " RESET_STYLE #var " = %li\n", \
            __DEBUGFILENAME__, __LINE__, __func__, (long int)var)
//! @copydoc __DEBUG_INT__
#define __DEBUG_DOUBLE__(file, type, var) \
    fprintf(file, BOLD type " %s@%d %s()]: " RESET_STYLE #var " = %f\n", \
            __DEBUGFILENAME__, __LINE__, __func__, (double)var)
//! @copydoc __DEBUG_INT__
#define __DEBUG_HEX__(file, type, var) \
    fprintf(file, BOLD type " %s@%d %s()]: " RESET_STYLE #var " = 0x%0X\n", \
            __DEBUGFILENAME__, __LINE__, __func__, var)

/**
 * Debug print.
 * @param[in] format Format formatting.
 * @param[in] ... Variables cited in the format formatting.
 * @return Number of characters printed.
 */
#define DEBUG_INFO(format, ...) \
    __DEBUG__(__DEBUG_INFO_FILE__, "[INFO", format, ##__VA_ARGS__)
/**
 * Debug print a variable.
 * @param[in] var Variable to be printed.
 * @return Number of characters printed.
 */
#define DEBUG_INFO_INT(var) __DEBUG_INT__(__DEBUG_INFO_FILE__, "[INFO", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_INFO_HEX(var) __DEBUG_HEX__(__DEBUG_INFO_FILE__, "[INFO", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_INFO_DOUBLE(var) \
    __DEBUG_DOUBLE__(__DEBUG_INFO_FILE__, "[INFO", var)

//! @copydoc DEBUG_INFO
#define DEBUG_WARN(format, ...) \
    __DEBUG__(__DEBUG_WARN_FILE__, YELLOW "[WARN", format, ##__VA_ARGS__)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_WARN_INT(var) \
    __DEBUG_INT__(__DEBUG_WARN_FILE__, YELLOW "[WARN", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_WARN_HEX(var) \
    __DEBUG_HEX__(__DEBUG_WARN_FILE__, YELLOW "[WARN", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_WARN_DOUBLE(var) \
    __DEBUG_DOUBLE__(__DEBUG_WARN_FILE__, YELLOW "[WARN", var)

//! @copydoc DEBUG_INFO
#define DEBUG_ERROR(format, ...) \
    __DEBUG__(__DEBUG_ERROR_FILE__, RED "[ERROR", format, ##__VA_ARGS__)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_ERROR_INT(var) \
    __DEBUG_INT__(__DEBUG_ERROR_FILE__, RED "[ERROR", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_ERROR_HEX(var) \
    __DEBUG_HEX__(__DEBUG_ERROR_FILE__, RED "[ERROR", var)
//! @copydoc DEBUG_INFO_INT
#define DEBUG_ERROR_DOUBLE(var) \
    __DEBUG_DOUBLE__(__DEBUG_ERROR_FILE__, RED "[ERROR", var)

#endif /* DEBUG_H */
