/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Single block definition.
 */

#ifndef BLOCK_H
#define BLOCK_H

#include <stdbool.h>
#include <stdint.h>

#include "cJSON.h"
#include "common.h"

//! Maximum message size.
#define MESSAGE_SIZE UINT8_C(100)

/**
 * Block definition.
 * @note The whole block is hashed.
 */
struct __attribute__((__packed__)) block_t {
    hash_t hash_previous; //!< Previous block hash, NULL if first.
    uint8_t nounce; //!< Number used only once.
    char message[MESSAGE_SIZE]; //!< Block message/content.
};
typedef struct block_t block_t;

/**
 * Print a single block.
 * @param[in] block Block to be printed.
 * @param[in] compact Display compact mode.
 */
void block_print(block_t block, bool compact);

/**
 * Get a new empty block from the heap.
 * @note This guarantees the initial block state to 0.
 * @param[in] message Message to initialize block with.
 * @return Block pointer.
 * @retval NULL Failed to allocate block.
 */
block_t* block_new(const char* message);

/**
 * Change message.
 * @param[in] block Block.
 * @param[in] message New message.
 */
void block_message(block_t* block, const char* message);

/**
 * Serialize block to json object.
 * @note User is responsible for the json object.
 * @param[in] block Block.
 * @param[out] json Writable json.
 * @return Success status.
 * @retval true Conversion successful.
 */
bool block_to_json(block_t block, cJSON* json);

/**
 * Read block from json file.
 * @param[in] block Block handle.
 * @param[in] json json object representing a block.
 * @return Success status.
 */
bool block_from_json(block_t* block, const cJSON* json);

#endif /* BLOCK_H */
