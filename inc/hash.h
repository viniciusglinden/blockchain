/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Wrapper for hash functions.
 */

#ifndef HASH_H
#define HASH_H

#include <stdint.h>

#include "block.h"

/**
 * Hash a block with SHA256 algorithm.
 * @param[in] block Block to be hashed.
 * @return Hash value.
 * @retval 0 if block points to NULL.
 */
hash_t hash_sha256(block_t* block);

#endif /* HASH_H */
