/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 */

#include <fcntl.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "blockchain.h"
#include "cJSON.h"
#include "code.h"
#include "common.h"

/**
 * Get bits mask.
 * @note Example:
 * @code{.c}
 * uint8_t ex = MASK_GET_LEFT(2, sizeof(uint8_t));
 * // ex = 0b11000000
 * @endcode
 * @param[in] ones Number of ones to the left.
 * @param[in] size Masks byte size.
 * @return Mask.
 */
#define MASK_GET_LEFT(ones, size) \
    (((2U << ((ones)-1U)) - 1U) << ((8U * (size)) - (ones)))

/**
 * Translate to valid or invalid with color.
 * @param[in] valid Valid variable.
 * @return String.
 */
#define BOOL_TO_VALID(valid) \
    ((valid) ? (GREEN "valid" RESET_STYLE) : (RED "invalid" RESET_STYLE))

blockchain_t blockchain_init(uint8_t zero_bits, hash_func_t hash_function) {
    if (zero_bits > 8 * sizeof(hash_t)) {
        ERROR_SET(parameter_invalid_e);
        zero_bits = 8 * sizeof(hash_t);
    }
    return (blockchain_t){.zero_bits = zero_bits,
                          .hash_function = hash_function,
                          .head = NULL,
                          .tail = NULL};
}

bool blockchain_block_add(blockchain_t* blockchain, char* message) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return false;
    }

    chain_t* chain = (chain_t*)calloc(1, sizeof(chain_t));
    if (!chain) {
        ERROR_SET(allocate_e);
        return false;
    }

    chain->block = block_new(message);

    if (!chain->block)
        return false;

    chain->previous = blockchain->tail;
    if (!chain->previous)
        // tail = NULL means blockchain is empty
        blockchain->head = chain;
    else {
        /*
         * if not empty:
         * 1. test if blockchain is valid
         * 2. input previous block hash into current block
         * 3. find suitable nouce for the blockchains zero bits / bit mask
         */
        if (!blockchain_validate(blockchain, 2)) {
            ERROR_SET(blockchain_not_valid_e);
            return false;
        }
        chain->block->hash_previous =
            blockchain->hash_function(chain->previous->block);

        const hash_t mask =
            MASK_GET_LEFT(blockchain->zero_bits, sizeof(hash_t));

        for (chain->block->nounce = 0;
             blockchain->hash_function(chain->block) & mask;
             chain->block->nounce++) {
            ;
            // TODO what if it does not find a valid hash?
        }
        blockchain->tail->next = chain;
    }
    blockchain->tail = chain;

    return true;
}

bool blockchain_validate(const blockchain_t* blockchain, uint8_t depth) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return false;
    }

    chain_t* chain = blockchain->tail;
    for (uint8_t i = 0; i < depth; i++) {
        if (!chain) // chain is empty
            return true;

        if (!chain->previous) // end of chain
            return (chain->block->hash_previous == 0);

        if (chain->block->hash_previous !=
            blockchain->hash_function(chain->previous->block))
            return false;

        chain = chain->previous;
    }

    return true;
}

void blockchain_print(const blockchain_t* blockchain, uint8_t index) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return;
    }

    chain_t* ptr = blockchain->tail;
    uint8_t current;
    hash_t hash;

    printf(BOLD "Blockchain {\t\n" RESET_STYLE
                "\tblockchain is %s,\n"
                "\tnumber of zero bits: %u,\n"
                "%12smask = 0b",
           BOOL_TO_VALID(blockchain_validate(blockchain, UINT8_MAX)),
           blockchain->zero_bits, " ");
    hash = 0xFF ^ MASK_GET_LEFT(blockchain->zero_bits, sizeof(hash_t));
    binary_print(hash, sizeof(hash));
    putchar('\n');
    for (current = 0; ptr && (current < index); ++current) {
        hash = blockchain->hash_function(ptr->block);
        printf("[%3u] ", current);
        if (ptr != blockchain->head) {
            printf("hash: 0x" HASH_SPECIFIER " = 0b", hash);
            binary_print(hash, sizeof(hash));
            printf(", ");
        } else {
            printf("%23s, ", "HEAD");
        }
        block_print(*ptr->block, true);
        ptr = ptr->previous;
    }
    if (current == index)
        printf("[...]\n");
    printf(BOLD "}\n" RESET_STYLE);
}

uint8_t blockchain_length(blockchain_t* blockchain) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return UINT8_MAX;
    }

    chain_t* ptr = blockchain->tail;
    uint8_t ret;
    for (ret = 0; (ptr != NULL) && (ret < UINT8_MAX); ++ret)
        ptr = ptr->previous;
    if (ptr) {
        ERROR_SET(maximum_reached_e);
        return UINT8_MAX;
    }
    return ret;
}

block_t* blockchain_block_get(blockchain_t* blockchain, int16_t index) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return NULL;
    }

    bool forwards = index >= 0;
    if (!forwards)
        index = -index;

    chain_t* ptr;

    ptr = forwards ? blockchain->head : blockchain->tail;

    for (uint8_t i = 0; (i < index) && ptr; i++)
        ptr = forwards ? ptr->next : ptr->previous;

    if (!ptr)
        return NULL;

    return ptr->block;
}

void blockchain_kill(blockchain_t blockchain) {
    chain_t* ptr = blockchain.tail;
    while (ptr) {
        blockchain.tail = blockchain.tail->previous;
        free(ptr->block);
        free(ptr);
        ptr = blockchain.tail;
    }
}

void blockchain_to_json(blockchain_t blockchain, const char* path) {
    FILE* fp = fopen(path, "w");
    if (!fp) {
        ERROR_SET(file_e);
        return;
    }
    cJSON* json = cJSON_CreateObject();
    if (!json) {
        fclose(fp);
        ERROR_SET(json_failed_e);
        return;
    }
    cJSON* blocks = cJSON_CreateArray();
    if (!blocks) {
        fclose(fp);
        cJSON_Delete(json);
        ERROR_SET(json_failed_e);
        return;
    }

    /**
     * @todo make it write the hash_function
     * This can be done in the following way:
     * - create a function that takes the hash_function pointer and compares to
     *   be memory
     * - it returns something similar to #var
     */
    cJSON_AddStringToObject(json, "hash function", "sha256");
    cJSON_AddNumberToObject(json, "zero bits", blockchain.zero_bits);
    cJSON_AddItemToObject(json, "blocks", blocks);

    chain_t* chain = blockchain.head;
    while (chain) {
        cJSON* block = cJSON_CreateObject();
        if (!block) {
            fclose(fp);
            cJSON_Delete(json);
            ERROR_SET(json_failed_e);
            return;
        }
        block_to_json(*chain->block, block);
        cJSON_AddItemToArray(blocks, block);
        chain = chain->next;
    }

    char* str = cJSON_Print(json);

    fputs(str, fp);
    cJSON_free(str);
    cJSON_Delete(json);
}

bool blockchain_from_json(blockchain_t* blockchain, const char* path) {
    if (!blockchain) {
        ERROR_SET(nullptr_e);
        return false;
    }
    char* contents = file_read(path);
    if (!contents) {
        ERROR_SET(file_e);
        return false;
    }
    cJSON *json = cJSON_Parse(contents);
    if (!json) {
        ERROR_SET(json_failed_e);
        return false;
    }

    cJSON* zero_bits = cJSON_GetObjectItemCaseSensitive(json, "zero bits");
    cJSON* blocks = cJSON_GetObjectItemCaseSensitive(json, "blocks");
    if ((!zero_bits) || (!blocks)) {
        ERROR_SET(json_failed_e);
        return false;
    }
    if ((!cJSON_IsNumber(zero_bits)) || (!cJSON_IsArray(blocks))) {
        ERROR_SET(json_failed_e);
        return false;
    }
    *blockchain = blockchain_init(zero_bits->valueint, hash_sha256);
    uint8_t length = cJSON_GetArraySize(blocks);

    for (uint8_t i = 0; i < length; ++i) {
        cJSON* item = cJSON_GetArrayItem(blocks, i);
        block_t* block = (block_t*)calloc(1, sizeof(block_t));
        if (!block) {
            ERROR_SET(allocate_e);
            return false;
        }
        chain_t* chain = (chain_t*)calloc(1, sizeof(chain_t));
        if (!chain) {
            ERROR_SET(allocate_e);
            return false;
        }
        if (!block_from_json(block, item)) {
            ERROR_SET(json_failed_e);
            return false;
        }

        chain->block = block;
        chain->previous = blockchain->tail;
        if (blockchain->tail)
            blockchain->tail->next = chain;
        blockchain->tail = chain;
        if (!blockchain->head)
            blockchain->head = chain;
    }

    cJSON_Delete(json);
    return true;
}
