/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-07-01
 */

#include <stddef.h>
#include <stdlib.h>

#include "code.h"
#include "io.h"

static bool io_generic(void* data,
                       size_t size,
                       const char* name,
                       access_t mode,
                       bool byte) {
    char mode_string[4] = {0};
    {
        uint8_t i = 0;
        if (mode == write_e)
            mode_string[i++] = 'w';
        else if (mode == append_e)
            mode_string[i++] = 'a';
        else
            mode_string[i++] = 'r';
        if (byte)
            mode_string[i++] = 'b';
    }
    FILE* fp = fopen(name, mode_string);
    if (!fp) {
        ERROR_SET(io_failed_e);
        return false;
    }

    if (mode == read_e)
        fread(data, size, 1, fp);
    else
        fwrite(data, size, 1, fp);

    fclose(fp);
    return true;
}

bool io_operation_byte(void* data,
                       size_t size,
                       const char* name,
                       access_t mode) {
    return io_generic(data, size, name, mode, true);
}

bool io_operation(const char* text, const char* name, access_t mode) {
    size_t size = strlen(text);
    if (size > MESSAGE_SIZE)
        size = MESSAGE_SIZE;
    return io_generic((void*)text, size, name, mode, false);
}
