/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Wrapper for hash functions.
 * @todo Offer algorithms from cmph simple C hash library.
 */

#include "hash.h"
#include "code.h"
#include "common.h"
#include "sha-256.h"

hash_t hash_sha256(block_t* block) {
    if (!block)
        return 0;

    uint8_t hash[SIZE_OF_SHA_256_HASH];
    calc_sha_256(hash, block, sizeof(block_t));

    return hash[0];
}
