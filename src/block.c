/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 */

#include <stdio.h>
#include <stdlib.h>

#include "block.h"
#include "code.h"

void block_print(block_t block, bool compact) {
    if (compact) {
        printf("nounce: 0x%02X - %s\n", block.nounce, block.message);
        return;
    }
    printf(BOLD "Block {\n" RESET_STYLE "\tprevious hash: 0x" HASH_SPECIFIER
                ",\n"
                "\tnounce: 0x" HASH_SPECIFIER ",\n"
                "\tmessage: %s\n" BOLD "}\n" RESET_STYLE,
           block.hash_previous,
           block.nounce,
           block.message);
}

block_t* block_new(const char* message) {
    block_t* block = (block_t*)calloc(1, sizeof(block_t));
    if (!block)
        ERROR_SET(allocate_e);
    else
        strncpy(block->message, message, MESSAGE_SIZE);
    return block;
}

void block_message(block_t* block, const char* message) {
    if (!block) {
        ERROR_SET(nullptr_e);
        return;
    }

    /*
     * Clean the whole message before writing user request.
     * This is because the user may write a shorter message.
     */
    memset(block->message, 0, MESSAGE_SIZE);
    strncpy(block->message, message, MESSAGE_SIZE);
}

bool block_to_json(block_t block, cJSON* json) {
    if (!json) {
        ERROR_SET(nullptr_e);
        return false;
    }
    // TODO make it write all the hash
    cJSON_AddNumberToObject(json, "hash previous", block.hash_previous);
    cJSON_AddNumberToObject(json, "nounce", block.nounce);
    cJSON_AddStringToObject(json, "message", block.message);

    return true;
}

bool block_from_json(block_t* block, const cJSON* json) {
    if ((!json) || (!block)) {
        ERROR_SET(nullptr_e);
        return false;
    }

    cJSON* hash_previous =
        cJSON_GetObjectItemCaseSensitive(json, "hash previous");
    cJSON* nounce = cJSON_GetObjectItemCaseSensitive(json, "nounce");
    cJSON* message = cJSON_GetObjectItemCaseSensitive(json, "message");
    if ((!hash_previous) || (!nounce) || (!message)) {
        ERROR_SET(json_failed_e);
        return false;
    }
    if ((!cJSON_IsNumber(hash_previous)) || (!cJSON_IsNumber(nounce)) ||
        (!cJSON_IsString(message))) {
        ERROR_SET(json_failed_e);
        return false;
    }

    block->hash_previous = hash_previous->valueint;
    block->nounce = nounce->valueint;
    strncpy(block->message, message->valuestring, MESSAGE_SIZE);

    return true;
}
