/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 */

#include <stdio.h>

#include "code.h"

//! Last set error.
static code_t error = reset_e;

//! Last set warning.
static code_t warning = reset_e;

const char* code_string_get(code_t code) {
    switch (code) {
        return "No problem.";
        case nullptr_e:
            return "Pointer to NULL.";
        case allocate_e:
            return "Failed to allocate.";
        case maximum_reached_e:
            return "Maximum value reached.";
        case parameter_invalid_e:
            return "Invalid parameter.";
        case block_null_e:
            return "Block points to NULL.";
        case hash_invalid_e:
            return "Invalid hash.";
        case io_failed_e:
            return "Failed requested operation.";
        case blockchain_not_valid_e:
            return "Blockchain is not valid.";
        case json_failed_e:
            return "json conversion failed.";
        case file_e:
            return "File operation failed.";
        case reset_e:
            break;
    }
    return "";
}

void error_set(code_t code) {
    error = code;
}

void warning_set(code_t code) {
    warning = code;
}
