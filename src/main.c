/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 */

#include <getopt.h>
#include <stdlib.h>

#include "block.h"
#include "blockchain.h"
#include "code.h"
#include "debug.h"
#include "git.h"
#include "hash.h"

//! Main blockchain.
static blockchain_t bc;

/**
 * Gracefully exit program.
 * @param[in] code Exit code.
 */
static void exit_now(int code) {
    blockchain_kill(bc);
    exit(code);
}

/**
 * Print error message to stderr and then exit.
 * @param[in] code Exit code.
 * @param[in] format Format formatting.
 * @param[in] ... Variables cited in the format formatting.
 */
#define PERROR(code, format, ...) \
    do { \
        fprintf(stderr, BOLD RED "error: " RESET_STYLE format "\n", \
                ##__VA_ARGS__); \
        exit_now(code); \
    } while (false)

static inline void example(void) {
    bc = blockchain_init(5, &hash_sha256);
    blockchain_block_add(&bc, "This is the first block being added");
    blockchain_block_add(&bc, "and this is the second");
    blockchain_block_add(&bc, "...");
    blockchain_block_add(&bc,
                         "Each block contains a writable message onto which "
                         "you can add some random text");
    blockchain_block_add(
        &bc,
        "When a new block is added, the last block message becomes permanent");
    blockchain_block_add(&bc, "This behaviour is verifiable.. let me show you");

    blockchain_print(&bc, UINT8_MAX);
    puts("Writing to valid.json...");

    block_t* block = blockchain_block_get(&bc, -3);
    if (!block)
        DEBUG_WARN("block points to NULL");
    block_message(block, BOLD "This is the adulterated message" RESET_STYLE
                              ", note the invalid hash");

    blockchain_print(&bc, 4);

    puts("Writing to invalid.json...");
    blockchain_to_json(bc, "invalid.json");
}

static inline void load(const char* path) {
    printf("Loading %s...\n", path);
    if (!blockchain_from_json(&bc, path))
        return;
    printf("Printing %s...\n", path);
    blockchain_print(&bc, 10);
}

static void help(const char* name) {
#define SPACE "  "
    printf("%s\n", git_version());
    printf(
        "\n"
        "Toy blockchain implementation\n"
        "\n"
        "USAGE\n"
        "\n" SPACE
        "%s [OPTIONS]\n"
        "\n"
        "OPTIONS\n"
        "\n" SPACE
        "-e,--example        Show an example.\n" SPACE
        "-p,--print=FILE     Print file.\n"
        "\n",
        name);
#undef SPACE
}

int main(int argc, char* argv[]) {
    static const struct option long_options[] = {
        {"help", no_argument, NULL, 'h'},
        {"example", no_argument, NULL, 'e'},
        {"print", required_argument, NULL, 'p'}};

    char option;

    while (true) {
        option = getopt_long(argc, argv, "hep:", long_options, NULL);
        if (option == -1)
            break;

        switch (option) {
            case 'h':
                help(argv[0]);
                exit_now(EXIT_SUCCESS);
                break;
            case 'e':
                example();
                break;
            case 'p':
                load(optarg);
                exit_now(EXIT_SUCCESS);
                break;
            default:
                PERROR(EXIT_FAILURE, "Unrecognized \'%c\' option", option);
                break;
        }
    };

    blockchain_kill(bc);
    return EXIT_SUCCESS;
}
