/**
 * @file
 * @author Vinícius Gabriel Linden
 * @date 2023-05-01
 * @brief Common definitions across the project.
 */

#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "code.h"

void array_print(const uint8_t array[], char* name, size_t size) {
    printf("%-20s[ ", name);
    for (size_t i = 0; i < size; ++i)
        printf("0x%02zX ", i);
    printf("]\n                      ");
    for (size_t i = 0; i < size; ++i)
        printf("0x%02X ", array[i]);
    printf("\n");
}

void binary_print(uint64_t number, size_t size) {
    size *= 8U;
    number &= (2U << ((size)-1U)) - 1U;
    for (size_t i = 0; i < size; ++i)
        putchar(((1U << (size - 1 - i)) & number) ? '1' : '0');
}

char* file_read(const char* path) {
    FILE* fp;

    fp = fopen(path, "r");
    if (!fp) {
        ERROR_SET(file_e);
        return NULL;
    }

    size_t size = 100;
    size_t i = 0;
    char* string = calloc(size, sizeof(char));
    char c;
    while (true) {
        if (i == size) {
            size *= 2;
            string = reallocarray(string, size, sizeof(char));
        }
        c = fgetc(fp);
        if (c == EOF)
            break;
        string[i++] = c;
    };
    string[i] = 0;

    fclose(fp);
    return string;
}
