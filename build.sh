#!/usr/bin/env bash
# shellcheck disable=SC2068
# Author Vinícius Gabriel Linden
# Date 2023-04-29

set -eu
set -o pipefail
shopt -s expand_aliases

alias cd="cd --"
alias rm="rm -rf --"

declare -r SCRIPT="${BASH_SOURCE[0]##*/}"
declare -r PROJECT_NAME="blockchain"
declare -A DIR
DIR[current]="$(readlink -f -- .)"
DIR[project]="$(dirname -- "$(readlink -f -- "${BASH_SOURCE[0]}")")"
DIR[build]="${DIR[project]}/cache"
readonly DIR
declare -A FILES
FILES[blockchain]="${DIR[build]}/${PROJECT_NAME}"
FILES[test]="${DIR[build]}/tests"
FILES[script]="$(readlink -f -- "${BASH_SOURCE[0]}")"
readonly FILES

source "${DIR[project]}/data/utils.sh"

shelltrap() {
    # necessary for automatic compilation
    echo
    _print information exiting at user request
    exit 0
}

trap shelltrap INT

clean() {
    case "$(_allowed_arguments "${1:-}" all build)" in
        all)
            _print information cleaning every generated binary or directory
            rm "${DIR[build]}" \
               "${DIR[project]}/gdb.log" \
               "${DIR[project]}/compile_commands.json" \
               "${DIR[project]}/output/" \
               "${DIR[project]}/"*".data" \
               "${DIR[project]}/Testing/"
            ;;
        "empty string"|build)
            _print information cleaning build directory
            rm "${DIR[build]}"
            ;;
        *)
            _print error invalid \""${1}"\" command
            return 1
            ;;
    esac
}

_cmake_configure() {
    mkdir -p "${DIR[build]}"
    cmake "-Wno-dev" "-S" "${DIR[project]}" "-B" "${DIR[build]}"
    _print information finished configuration phase
}

_cmake_build() {
    cmd=("cmake" "--build" "${DIR[build]}")

    [[ "${#}" -ne 0 ]] &&
        cmd+=("--target" "$1")

    "${cmd[@]}"
}

build() (
    declare target
    case "$(_allowed_arguments "${1:-}" all blockchain bc tests test doc)" in
        "empty string"|blockchain|bc)
            _print information target is blockchain
            target="blockchain"
            ;;
        test*)
            _print information target is tests
            target="tests"
            ;;
        doc)
            _print information target is documentation
            target="doc"
            ;;
        all)
            _print information target is blockchain and tests
            ;;
        *)
            _print error invalid \""${1}"\" command
            return 1
            ;;
    esac
    _cmake_configure
    _cmake_build ${target:-}
)

doc() {
    _print information building code documentation
    build doc
    index=("$(find "${DIR[build]}" -type f -name "index.html")")
    _open "${index[0]}"
}

run() {
    _test_build() {
        [[ -f "${FILES[${1}]}" ]] &&
            return 0
        _print information file is missing, building it first &&
            build "${1}"
    }
    msg=(information running)
    local -a cmd
    case "$(_allowed_arguments "${1:-}" \
        blockchain bc \
        test tests \
        ctest ctests \
        )" in
        "empty string"|blockchain|bc)
            msg+=("target binary")
            _test_build blockchain
            cmd=("${FILES[blockchain]}" "--example") # temporary
            ;;
        test*)
            msg+=("test binary")
            _test_build test
            cmd=("${FILES[test]}" "--success")
            ;;
        ctest*)
            msg+=("ctest command")
            _test_build test
            cmd=("ctest" "--test-dir" "${DIR[build]}")
            ;;
        *)
            _print error invalid \""${1}"\" command
            return 1
            ;;
    esac
    _print "${msg[@]}"
    "${cmd[@]}"
}

auto() {
    read -ra COMMANDS <<< "${@//${FUNCNAME[0]}}"
    command -v entr >/dev/null ||
        (echo "Install \`entr\` first!" &&
        return 1)
    while true; do
        find \
            "${DIR[project]}/src/" \
            "${DIR[project]}/inc/" \
            "${DIR[project]}/test/" \
            -iname "*.c" -o -iname "*.h" -o \
            -iname "*.cpp" -o -iname "*.hpp" \
                | entr -c sh -c -- "echo 'Automatic running:' ${COMMANDS[*]} \
                '\nPress ^C to exit, \`q\` to force refresh'; \
                date; \
                ${BASH_SOURCE[0]} ${COMMANDS[*]}" || \
                true
    done
    # should never reach here
    exit 1
}

gdb() {
    [[ "$(_inside_container)" -eq 1 ]] &&
        _print attention container does not have "${FUNCNAME[0]}", aborting \
            function &&
        return 0
    command gdb --silent "${FILES[blockchain]}"
}

valgrind() {
    command valgrind \
        --leak-check=full --track-origins=yes --error-exitcode=1 -s \
        "${FILES[blockchain]}" --example
    _print attention no error
}

container() (
    [[ "$(_inside_container)" -eq 1 ]] &&
        _print information already inside a container &&
        return 0
    _print information running script inside the container
    _container "${BASH_SOURCE[0]}" "${@}"
    exit 0
)

install() {
    [[ ! -d "${DIR[build]}" ]] &&
        _print error nothing was built, aborting function &&
        return 1
    [[ "${DESTINATION:+${DESTINATION}}" ]] ||
        DESTINATION="${DIR[current]}"
    mkdir -p "${DESTINATION}"
    _print information installing generated files to "${DESTINATION}"
    cmake --install "${DIR[build]}" --prefix "${DESTINATION}"
}

format() {
    _print information formating code
    clang-format -i \
        "${DIR[project]}/inc/"* \
        "${DIR[project]}/src/"* \
        "${DIR[project]}/test/"*
}

ci-build() {
    _print information running CI \"build\" stage
    "${FILES[script]}" clean=all build=blockchain valgrind install
}

ci-test() {
    _print information running CI \"test\" stage
    "${FILES[script]}" clean=all build=test run=test run=ctest install
}

hooks() {
    _print information copying hooks
    cp -rp "${DIR[project]}/data/pre-push" "${DIR[project]}/.git/hooks/"
}

help() {
    cat << EOF

Automation for project generation, documentation and execution.

USAGE

    ${SCRIPT} [COMMAND=[OPTION[,OPTION]]] [COMMAND=[OPTION[,OPTION]]...

COMMAND

    clean[=OPTION]          Clean OPTION.
                            Options are: all, build [default].
    build[=TARGET]          Build TARGET.
                            Targets are: b[lock]c[hain], test[s], doc.
                            Default: blockchain and test[s].
    doc                     Generate code documentation. If it detect an
                            "open" command, the main page will automatically
                            be opened.
    run[=TARGET]            Run TARGET. If TARGET does not exit, it will first
                            build it.
                            Targets are: b[lock]c[hain] [default], test[s],
                            ctest[s].
    auto                    Run the next commands automatically when changing
                            \`.c\`, \`.h\`, \`.cpp\`, \`.hpp\` inside \`src/\`,
                            \`inc/\` and \`test/\` \`folders.txt\`.
                            This required \`entr\`.
    gdb                     Run blockchain inside gdb.
    valgrind                Run valgrind memory check on blockchain.
                            This exits the script on valgrind error.
    container               Run the following commands inside a container.
                            This requires either \`podman\` or \`docker\`.
    install                 Install available generated binary and documentation
                            to current folder.
                            Note: export DESTINATION variable to another folder
                            when desired.
    format                  Format code. Requires \`clang-format\`.
    hooks                   Install git hooks.

SHORTHAND

    \`run\`
      bc,blockchain         build=blockchain run=blockchain
      test                  build=test run=test

EXAMPLE

    ${BASH_SOURCE[0]} clean=all build run
        This command will clean all cache files, then build the binary
        and then run it.

    ${BASH_SOURCE[0]} run=bc,tests
        This command will run blockchain and then tests.
        Upon absent of such executables, they will automatically be built.

EOF
}

_main() {
    (
        cd "${DIR[project]}"
        git submodule update --init --recursive > /dev/null
    )

    [[ "${#}" -eq 0 ]] &&
        _print information nothing to run &&
        exit 0

    # user can give options via <command>=<option>
    while [[ "${#}" -ne 0 ]]; do
        IFS="=" read -r command arguments <<< "${1}"
        IFS="," read -ra arguments <<< "${arguments[@]}"
        local message=(calling "\`${command}\`")
        [[ "${#arguments[@]}" -ne 0 ]] &&
            message+=(with "\`${arguments[*]}\`" as arguments)
        _print information "${message[@]}"

        case "${command}" in
            -h|--help|help)
                help
                exit 0
                ;;
            clean)
                _repeat clean ${arguments[@]}
                ;;
            build)
                _repeat build ${arguments[@]}
                ;;
            run)
                _repeat run ${arguments[@]}
                ;;
            auto)
                auto ${@}
                ;;
            bc|blockchain)
                build bc
                run bc
                ;;
            test)
                build test
                run test
                ;;
            container)
                shift
                container ${@}
                ;;
            _*)
                _print error command not allowed
                exit 1
                ;;
            *)
                type "${command}" &>/dev/null || {
                    _print error command not found
                    exit 1
                }
                "${1}"
                ;;
        esac
        shift
    done
}

_main "${@}"

