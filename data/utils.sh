#!/usr/bin/env bash
# Author Vinícius Gabriel Linden
# Date 2023-04-29
# Note provide PROJECT_NAME variable before sourcing

set -eu
set -o pipefail

declare -r RESET_STYLE="\033[0m"
declare -r BLACK="\033[30m"
declare -r RED="\033[31m"
declare -r GREEN="\033[32m"
declare -r YELLOW="\033[33m"
declare -r BLUE="\033[34m"
declare -r MAGENTA="\033[35m"
declare -r CYAN="\033[36m"
declare -r WHITE="\033[37m"
declare -r BOLD="\033[1m"

_color_get() {
    for i in "${@}"; do
        case "${i,,}" in
            black)
                echo -en "${BLACK}" ;;
            error|red)
                echo -en "${RED}" ;;
            green|info*)
                echo -en "${GREEN}" ;;
            yellow|warn*)
                echo -en "${YELLOW}" ;;
            blue)
                echo -en "${BLUE}" ;;
            magenta)
                echo -en "${MAGENTA}" ;;
            cyan|att*)
                echo -en "${CYAN}" ;;
            white)
                echo -en "${WHITE}" ;;
            bold)
                echo -en "${BOLD}" ;;
            reset)
                echo -en "${RESET_STYLE}" ;;
            *)
                return 1 ;;
        esac
    done
}

_print() {
    # FIX-ME ./build.sh clean=all run will print
    # "[blockchain clean]: file is missing, building it first"
    [[ "${#}" -lt 2 ]] &&
        return 1
    _color_get "${1}" bold
    shift
    [[ "${FUNCNAME[1]}" =~ ^_ ]] || \
        FUNCTION=" ${FUNCNAME[1]}"
    echo -e "[${PROJECT_NAME}${FUNCTION:-}]: ${RESET_STYLE}${*}"
}

_open() {
    # This is a non-essential command, it is ok if if does not work
    local cmd
    if command -v xdg-open &>/dev/null; then
        cmd="xdg-open"
    elif command -v open &>/dev/null; then
        cmd="open"
    elif command -v cygstart &>/dev/null; then
        cmd="cygstart"
    fi
    [[ "${cmd}" == "" ]] &&
        _print warning no open tool found &&
        return 0
    for i in "${@}"; do
        exec "${cmd}" "${i}" ||
            return 0
    done
}

_allowed_arguments() {
    # usage: _allowed_arguments searched option1 option2...
    # example: option=$(_allowed_arguments "${1:-}" option1 option2)
    # first element is the given parameter
    # other elements are the allowed parameters
    # if searched in empty: echos "empty string" and returns 0
    # if searched is found: echos the option back and returns 0
    # if searched is not found: echos "not found" and returns 0
    # if no option is given: echos "no option" and returns 1
    search="${1:-}"
    [[ "${search}" == "" ]] &&
        echo "empty string" &&
        return 0
    shift
    [[ "${#}" -lt 1 ]] &&
        echo "no option" &&
        return 1
    for option in "${@}"; do
        [[ "${option}" == "${search}" ]] &&
            echo "${option}" &&
            return 0
    done
    echo "not found"
}

_inside_container() {
    # FIX-ME this only works for podman
    [[ -f "/run/.containerenv" ]] &&
        echo 1 ||
        echo 0
}

_container() {
    declare -a cmd
    if command -v podman &>/dev/null; then
        cmd+=("podman")
    elif command -v docker &>/dev/null; then
        cmd+=("docker")
    else
        _print error please install either podman or docker
    fi
    cmd+=("run"
          "--rm"
          "-it"
          "-v" "$(pwd):/data/"
          # "-u$(id -u):$(id -g)"
          "registry.gitlab.com/viniciusglinden/blockchain:debian"
          "${@}")
    "${cmd[@]}"
}

_repeat() {
    [[ "${#}" -le 1 ]] &&
        "${1}" &&
        return 0

    for argument in "${@:2}"; do
        "${1}" "${argument}"
    done
}
